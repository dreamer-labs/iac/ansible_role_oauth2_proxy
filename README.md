# ansible_role_oauth2_proxy

Provides a clean method of installing and configuring oauth2-proxy

| Variable                                | Default Value                | Explanation                                   |
|:----------------------------------------|:-----------------------------|:----------------------------------------------|
| oauth2_proxy_upstreams                  |                              | Required: List of upstream http(s) urls       |
| oauth2_proxy_client_id                  |                              | Required: oauth2 client ID from your OIDP     |
| oauth2_proxy_client_secret              |                              | Required: oauth2 client secret your OIDP      |
| oauth2_proxy_cookie_secret              |                              | Required: cookie secret random string         |
| oauth2_proxy_issuer_url                 |                              | Required: URL (full path) to the oidc issuer  |
| oauth2_proxy_redirect_url               |                              | Required: Redirect URL (full path)            |
| oauth2_proxy_service_name               | oauth2_proxy                 | Allow for multiple systemd service files to support multiple backends |
| oauth2_proxy_config_file                | /etc/oauth2_proxy.cfg        | Allow for multiple config files to support multiple backends |
| oauth2_proxy_version                    | v7.1.3                       | Install this version of oauth2-proxy          |
| oauth2_proxy_email_domains              | ["*"]                        |                                               |
| oauth2_proxy_listen_https               | False                        | Boolean to control listening on HTTPS         |
| oauth2_proxy_listen_address             | "127.0.0.1:4180"             | Listen address of the oauth2-proxy server     |
| oauth2_proxy_tls_key_file               |                              | Path to file for the TLS key. Must be provided if `oauth2_proxy_listen_https` is set to true |
| oauth2_proxy_tls_cert_file              |                              | Path to file for the TLS cert. Must be provided if `oauth2_proxy_listen_https` is set to true |
| oauth2_proxy_provider                   | oidc                         | oauth2-proxy provider type                    |
| oauth2_proxy_log_dir                    | /var/log/oauth2-proxy        | log directory                                 |
| oauth2_proxy_log_file                   | oauth2-proxy.log             | log file                                      |
| oauth2_proxy_oidc_groups_claim          | (see oauth2-proxy docs)      | oidc group claim field to restrict groups     |
| oauth2_proxy_allowed_groups             |                              | list of allowed groups to restrict auth for   |
| oauth2_proxy_oidc_scope                 | "openid email profile"       | Space-delimited list of scopes to request from IDP |
