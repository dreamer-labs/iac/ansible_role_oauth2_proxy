import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_service_running_and_enabled(ansible_vars, host):

    for service_name in [var['oauth2_proxy_service_name'] for var in ansible_vars]:
        service = host.service(service_name)
        assert service.is_running
        assert service.is_enabled


def test_port_listening(ansible_vars, host):

    for socket_addr in [var['oauth2_proxy_listen_address'] for var in ansible_vars]:

        assert host.socket(f"tcp://{socket_addr}")
