import os
import pytest
import yaml


@pytest.fixture
def ansible_vars():
    ansible_vars = []

    pth = os.path.realpath(
        os.path.join(__file__, "..", "..", "converge.yml")
    )

    defaults = os.path.realpath(
        os.path.join(__file__, "..", "..", "..", "..", "defaults/", "main.yml")
    )

    with open(pth) as converge_file, open(defaults) as defaults_file:
        default_vars = yaml.safe_load(defaults_file)
        for include_role in yaml.safe_load(converge_file)[0]["tasks"]:
            merged_vars = {**default_vars, **include_role["vars"]}
            ansible_vars.append(merged_vars)

    return ansible_vars
