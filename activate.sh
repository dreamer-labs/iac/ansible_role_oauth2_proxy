#!/bin/sh
( mkdir molecule || echo ""
cd molecule/
git clone https://gitlab.com/dreamer-labs/iac/playbooks.git common-src )
ln -s $PWD/molecule/common-src/openstack-linux $PWD/molecule/common/playbooks
ls -l molecule/common molecule/common/playbooks
